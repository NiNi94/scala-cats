plugins {
    scala
    `java-library`
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.scala-lang:scala-library:2.13.3")
    implementation("org.typelevel:cats-core_2.13:2.2.0")
}
