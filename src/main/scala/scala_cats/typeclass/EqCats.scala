package scala.catz.typeclass

object EqCats extends App {
  import cats.Eq
  import cats.syntax.eq._
  import cats.syntax.option._
  import cats.instances.int._
  import cats.instances.option._
  println(123 === 123)
  println(123 == Option(123))
  println(1.some === 1.some)

  case class Cat(name: String)
  //implicit val eqCat: Eq[Cat] = (x: Cat, y: Cat) => x.name == y.name
  implicit val eqCat2: Eq[Cat] = Eq.instance[Cat]((cat1, cat2) => cat1.name == cat2.name)
  println(Cat("bruno") === Cat("mario"))
}
