package scala_cats.typeclass

trait Printable[A] {
  def format(elem: A): String
}

object PrintableInstances {
  implicit val printableString: Printable[String] = (elem: String) => elem
  implicit val printableInt: Printable[Int] = (elem: Int) => elem.toString
  implicit val printableCat: Printable[Cat] = (cat: Cat) =>
    s"${cat.name} is a ${cat.age} year-old ${cat.color} cat"
}

object Printable {
  def format[A](elem: A)(implicit printable: Printable[A]): String = printable.format(elem)
  def print[A](elem: A)(implicit printable: Printable[A]): Unit = println(printable.format(elem))
}

object PrintableSyntax {
  implicit class PrintableOps[A](elem: A) {
    def format(implicit printable: Printable[A]): String = printable.format(elem)
    def print(implicit printable: Printable[A]): Unit = println(printable.format(elem))
  }
}

final case class Cat(name: String, age: Int, color: String)

object UseCat extends App {
  import PrintableInstances._
  import PrintableSyntax._
  val cat = Cat("Bruno", 12, "red")
  Printable.print(cat)
  cat.print
}

import cats.Show
import cats.instances.int._
import cats.syntax.show._
object ShowCat extends App {
  case class Dog(name: String)
  //implicit val dogShow: Show[Dog] = (dog: Dog) => s"Dog name is ${dog.name}"
  implicit val dogShow2: Show[Dog] = Show.show[Dog]((dog: Dog) => s"Dog name is ${dog.name}")
  println(123.show)
  println(Dog("molly").show)
}

object CatShow extends App {
  implicit val catShow: Show[Cat] = Show.show[Cat](cat => s"${cat.name} is a ${cat.age} year-old ${cat.color} cat")
  val cat = Cat("Bruno", 12, "red")
  println(cat.show)
}