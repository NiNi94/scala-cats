package scala_cats.functors

object Functors extends App {
  import cats.Functor
  import cats.instances.list._
  import cats.instances.option._
  import cats.syntax.option._

  val list1 = List(1, 2, 3)
  private val list2 = Functor[List].map(list1)(_ * 2)
  println(list2)
  private val opt1 = 2.some
  private val opt2 = Functor[Option].map(opt1)(_.toString)
  println(opt2)
  private val func = (x: Int) => x + 1
  private val liftedFunc = Functor[Option].lift(func)
  println(liftedFunc)
  private val optLifted = liftedFunc(Option(2))
  println(optLifted)
}

object FunctorSyntax extends App {
  import cats.instances.function._
  import cats.syntax.functor._
  val func1 = (a: Int) => a + 1
  val func2 = (a: Int) => a * 2
  val func3 = (a: Int) => s"$a!"
  val func4 = func1.map(func2).map(func3)
  println(func4(123))
}

object Math extends App {
  import cats.Functor
  import cats.syntax.functor._
  import cats.syntax.option._
  implicit val optionFunctor: Functor[Option] = new Functor[Option] {
    override def map[A, B](value: Option[A])(f: A => B): Option[B] = value.map(f)
  }
  def doMath[F[_]](start: F[Int])(implicit functor: Functor[F]): F[Int] = start.map(n => n + 1 * 2)
  println(doMath(20.some))

  import scala.concurrent.{ExecutionContext, Future}
  implicit def futureFunctor(implicit ec: ExecutionContext): Functor[Future] = new Functor[Future] {
    override def map[A, B](value: Future[A])(f: A => B): Future[B] = value.map(f)
  }
}

object Tree extends App {
  import cats.Functor
  sealed trait Tree[+A]
  final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
  final case class Leaf[A](value: A) extends Tree[A]

  implicit val treeFunctor: Functor[Tree] = new Functor[Tree] {
    override def map[A, B](tree: Tree[A])(f: A => B): Tree[B] = tree match {
      case Branch(left, right) => Branch(map(left)(f), map(right)(f))
      case Leaf(value) => Leaf(f(value))
    }
  }
  import cats.syntax.functor._
  import cats.syntax.option._
  val tree1: Tree[Int] = Branch(Branch(Leaf(2), Leaf(3)), Branch(Leaf(3), Leaf(5)))
  val treeOption = tree1.map(_.some)
  println(treeOption)
}
