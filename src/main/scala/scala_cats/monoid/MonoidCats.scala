package scala_cats.monoid

object MonoidIntro {
  trait Semigroup[A] {
    def combine(x: A, y: A): A
  }
  trait Monoid[A] extends Semigroup[A] {
    def empty: A
  }
  object Monoid {
    private def apply[A](implicit monoid: Monoid[A]) =
      monoid
  }

  class BoolMonoidAnd extends Monoid[Boolean] {
    override def empty: Boolean = false
    override def combine(x: Boolean, y: Boolean): Boolean = x && y
  }

  class BoolMonoidOr extends Monoid[Boolean] {
    override def empty: Boolean = false
    override def combine(x: Boolean, y: Boolean): Boolean = x || y
  }

  class SetMonoidIntersect[T] extends Monoid[Set[T]] {
    override def empty: Set[T] = Set.empty
    override def combine(x: Set[T], y: Set[T]): Set[T] = x & y
  }
}

object MonoidCats extends App {
  import cats.Monoid
  import cats.syntax.option._
  import cats.syntax.monoid._
  val hello = Monoid[String].combine("ciao", "hello")
  println(hello)
  val opt = Monoid[Option[String]].combine("bye".some, "bye".some)
  println(opt)
  val hello2 = "ciao" |+| "hello" |+| Monoid[String].empty
  println(hello2)
}

object MonoidExercise extends App {
  import cats.Monoid
  import cats.syntax.monoid._
  def add[A: Monoid](items: List[A]): A = items.foldLeft(Monoid[A].empty)(_|+|_)

  final case class Cat(name: String)
  implicit val listIntMonoid: Monoid[Cat] = Monoid.instance[Cat](Cat(""), (cat1, cat2) => Cat(cat1.name + cat2.name))
  val cat1 = Cat("Blue")
  val cat2 = Cat("Jonny")
  val catList = List[Cat](cat1, cat2)
  println(add[Cat](catList))
}
