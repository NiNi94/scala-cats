package scala_cats.monads

object MyMonad {
  trait MyMonad[F[_]] {
    def pure[A](a: A): F[A]
    def flatMap[A, B](value: F[A])(func: A => F[B]): F[B]
    def map[A, B](value: F[A])(func: A => B): F[B] = flatMap(value)(a => pure(func(a)))
    // Mapping a: A -> b: B e poi wrapping semplice del risultato della funzione (vogliamo essere conformi a F[B]).
    // Il wrapping semplice di un b:B -> F[B] si fa con pure!
  }
}

object TryMonad extends App {
  import cats.Monad
  import cats.instances.option._
  import cats.syntax.option._

  val opt1 = Monad[Option].pure(3)
  val opt2 = Monad[Option].flatMap(opt1)(a => (a + 2).some)
  val opt3 = Monad[Option].map(opt2)(a => 100 * a)
  println(opt3)
  val list1 = Monad[List].pure(5)
  val list2 = Monad[List].flatMap(List(1, 2, 3))(a => List(a, a * 10))
  val list3 = Monad[List].map(list2)(a => a + 123)
  println(list3)

  import cats.instances.option._
  Monad[Option].flatMap(1.some)(a => (a * 2).some)
  import cats.instances.list._
  Monad[List].flatMap(List(1, 2, 3))(a => List(a * 10))
  import cats.instances.vector._
  Monad[Vector].flatMap(Vector(1, 2, 3))(a => Vector(a, a * 10))

  import cats.instances.future._

  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent._
  import scala.concurrent.duration._
  val fm = Monad[Future]
  val future = fm.flatMap(fm.pure(1))(x => fm.pure(x + 2))
  Await.result(future, 1.second)

  import cats.instances.list._
  import cats.instances.option._
  import cats.syntax.applicative._ // for pure
  val one = 1.pure[Option]
  val list = 1.pure[List]
  println(one, list)
}

object MonadDemonstration extends App {
  import cats.Monad
  import cats.syntax.flatMap._
  import cats.syntax.functor._

  def sumSquare[F[_] : Monad](a: F[Int], b: F[Int]): F[Int] = a.flatMap(x => b.map(y => x * x + y * y))
  import cats.instances.list._
  import cats.instances.option._
  import cats.syntax.option._
  sumSquare(3.some, 4.some)
  val res2 = sumSquare(List(1, 2, 3), List(4, 5))
  println(res2)
  def sumSquare2[F[_] : Monad](a: F[Int], b: F[Int]): F[Int] = for {
    x <- a
    y <- b
  } yield x * x + y * y
  import cats.Id
  val res3 = sumSquare(3: Id[Int], 4: Id[Int])
  println(res3)
}

object IDMonad extends App {
  import cats.syntax.flatMap._
  import cats.syntax.functor._
  import cats.{Id, Monad}
  val a = Monad[Id].pure(3)
  val b = Monad[Id].flatMap(a)(_ + 1)
  println(b)
  val res1 = for {
    x <- a
    y <- b
  } yield x + y
  println(res1)
}

object IDMonadEx extends App {
  import cats.Id
  def pure[A](value: A): Id[A] = value
  def map[A, B](initial: Id[A])(func: A => B): Id[B] = func(initial)
  def flatMap[A, B](initial: Id[A])(func: A => Id[B]): Id[B] = func(initial)
}